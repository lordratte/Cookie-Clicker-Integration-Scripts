(function() {

  function payload() {
    // Game can only be accessed from in here possibly with window.Game
    window.addEventListener("message", function(event) { //This lets us get info from other places in this file -- for example, the results from the request to Duolinguo's server
      if (event.source != window) return;
      console.log(event.data);
    }
  }


  // ===== Once-off actions =====
  if (!window.ALREADY_INJECTED) {
    window.ALREADY_INJECTED = true;

    window.addEventListener("message", function(event) { // This listens for messages from our injected code
      if (event.source != window) return;
      console.log(event.data);
    }, false);

    chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) { // This listens for messages from anywhere else in the plugin
    });

    // Install payload to give it access to the page's global variables like Game
    var code = '(' + payload + ')();';
    var script = document.createElement('script');
    script.textContent = code;
    (document.head||document.documentElement).appendChild(script);
    script.remove();
  }

  // ===== Repeated actions =====
  window.postMessage({type: "ping"}, "*");
})();
